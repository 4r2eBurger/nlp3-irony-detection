import nltk
from nltk.tokenize import casual,TweetTokenizer
from nltk.probability import FreqDist
import numpy as np
import operator
import utils
from math import log
import re
from nltk.util import bigrams

nltk.download('stopwords') # If needed
nltk.download('punkt') # If needed
nltk.download('wordnet') # If needed
nltk.download('averaged_perceptron_tagger')# If needed
utils.install('emoji')
import emoji
# retain case-sensitivity in emoticons
tw_tknz = TweetTokenizer(strip_handles=True,preserve_case=False)
lemmatizer = nltk.stem.WordNetLemmatizer()
# retain commas, full stops, ellipsis and exclamation
stopwords=set(nltk.corpus.stopwords.words('english'))
stopwords= stopwords.union(["--","``","''","'","(",")","|","-","`","'","..",":","&","\"","\/","\\"])
# tweet pos tagset : http://www.cs.cmu.edu/~ark/TweetNLP/gimpel+etal.acl11.pdf
POS_TAGSET = ["N", "O", "S", "^", "Z" ,"L", "M", "V", "A", "R", "!", "D", "P", "&", "T", "X", "Y", "#", "@", "~", "U", "E", "$", ",", "G", "EJ"]

# sentiment lexicon directory
SENTI_DIR = "../toolkit/sentiment_lexicon/"
AFINN = "AFINN/AFINN-en-165.txt"
EMOTIC_LEX = "AFINN/AFINN-emoticon-8.txt"
EMOJI_LEX = "emoji_senti/Emoji_Sentiment_Data_v1.0.csv"

EMOJI = "emoji"
EMOTIC= "emoticon"
PUNCTUATION = "punctuation"
HASHTAG = "hashtag"
WORDS = "words"

FEATURED_TYPE = [EMOJI,EMOTIC,PUNCTUATION,HASHTAG,WORDS]

preserved_punc = ['.','!','...','?',',']

def get_tweet_token(string):
  return [lemmatizer.lemmatize(token) 
  for token in tw_tknz.tokenize(string) 
  if not re.search(casual.URLS,token) 
  and token not in stopwords 
  and not token.startswith("http")
  ]

def get_list_tokens(tweets,labels):
  irony_tkns = []
  non_irony_tkns = []
  for i in range(len(tweets)):
    tw_tkn = get_tweet_token(tweets[i])
    if labels[i] == 1:irony_tkns.append(tw_tkn)
    else: non_irony_tkns.append(tw_tkn)
  return irony_tkns,non_irony_tkns

def get_full_tokens(raw):
  tokens = []
  for i in range(len(raw)):
    tokens.append(get_tweet_token(raw[i]))
  return tokens


def emoji_decoder(string):
    return emoji.demojize(string)

def get_featured_token_list(token_list):
    # emojis
    emojis = []
    # emoticons
    emoticons = []
    # punctuation
    punctuations = []
    # hashtags
    hashtags = []
    # words
    words = []
    for sent_list in token_list:
      for token in sent_list:
        if emoji.demojize(token) != token: emojis.append(token)
        elif casual.EMOTICON_RE.search(token): emoticons.append(token)
        elif token in preserved_punc: punctuations.append(token)
        elif token[0]=='#' and len(token)>1: hashtags.append(token)
        elif token != '#': words.append(token)
    return {EMOJI:emojis,EMOTIC:emoticons,PUNCTUATION:punctuations,HASHTAG:hashtags,WORDS:words}

def get_freq_dist(token_list=[],dim = 1):
  if dim > 1:
    fdist = FreqDist()
    for sent_tkn in token_list:
        for token in sent_tkn:
          fdist[token] += 1
    return fdist
  elif dim<=1:
    return FreqDist(token_list)

def get_freq_dict(freq_dist):
  return dict(freq_dist.most_common(freq_dist.N()))

def get_bigrams(token_lists=[]):
  tkns_seq = []
  for sent_tokens in token_lists:
    for tkn in sent_tokens:
      tkns_seq.append(tkn)
  return bigrams(tkns_seq)

def load_pos(file):
    tweet_pos_list = []
    with open(file,'r', encoding='utf-8',
                 errors='ignore') as f:
      raw =  f.readlines()
      pos_vec = np.zeros(26)
      for line in raw:
        if line =='\n':
           tweet_pos_list.append(pos_vec)
           pos_vec = np.zeros(26)
           continue
        split = line.split('\t')
        #print (split)
        tkn = split[0]
        tag = split[1]
        # fixing tag of emoji and hashtags
        if emoji.demojize(tkn) != tkn: 
           pos_vec[25] += len(tkn)
        elif tkn[0]=='#':
           pos_vec[POS_TAGSET.index("#")] += len(tkn.split('#'))
        else:
          pos_vec[POS_TAGSET.index(tag)] += 1
      return tweet_pos_list

def load_senti_lexicon(filename):
  with open(SENTI_DIR+filename,'r', encoding='utf-8',
                 errors='ignore') as f:
    afinn = dict(map(lambda x: (x[0],int(x[1])),
                [ line.split('\t') for line in f.readlines() ]))
    return afinn

def load_sentiemo_lexicon():
    # score = (p+n)/m
  with open(SENTI_DIR+EMOJI_LEX,'r', encoding='utf-8',
                 errors='ignore') as f:
    emo_senti = dict(map(lambda x: (x[0],round((float(x[6])-float(x[4]))/float(x[2]),3)),
                [line.split(',') for line in f.readlines()]))
    return emo_senti

def get_senti_features(tokens):
  senti_vec_list = []
  senti_dic = load_senti_lexicon(AFINN)
  emoticon_dic = load_senti_lexicon(EMOTIC_LEX)
  sentiemo_dic = load_sentiemo_lexicon()
  # positive negative neutral words averaged over text length
  for sent in tokens :
    senti_vec = np.zeros(6)
    length = len(sent)
    highest = 0
    lowest = 0
    contrast_flag = 0
    pre_val=0
    for i,tkn in enumerate(sent):
      tkn_senti_val = 0
      # words sentiment matching
      if tkn in senti_dic:
        tkn_senti_val = senti_dic[tkn]
      elif tkn in emoticon_dic:
        tkn_senti_val = emoticon_dic[tkn]
      # emoji sentiment matching
      elif tkn in sentiemo_dic:
        tkn_senti_val = sentiemo_dic[tkn]

      if tkn_senti_val>0:
          senti_vec[0] +=1 # positive
      elif tkn_senti_val<0:
        senti_vec[1] +=1 # negative
      else: senti_vec[2] +=1 # neutral

      # overall polarity (the sum of the values of the identified sentiment words)
      senti_vec[3]+=tkn_senti_val
      if tkn_senti_val>highest:highest=tkn_senti_val
      elif tkn_senti_val<lowest:lowest=tkn_senti_val
      # binary feature of sentiment clash
      if contrast_flag == 0 and pre_val * tkn_senti_val >=0:
        pre_val = tkn_senti_val
      elif pre_val * tkn_senti_val <0:
        contrast_flag=1
    # diff of highest and lowest
    senti_vec[4] = highest - lowest
    senti_vec[5] = contrast_flag
    senti_vec_list.append(senti_vec)
  return senti_vec_list



def freq_scaler(collection_freq,topic_freq_list):
    # scaling token frequencies according to topic relevance
    for i in range(len(topic_freq_list)):
        freq_dic = topic_freq_list[i]
        for token in freq_dic:  
            df = 2 if token in topic_freq_list[0]  and token in  topic_freq_list[1] else 1
            i_tf = freq_dic[token]
            i_cf = collection_freq[token]
            # cacluate weighting
            i_tf_idf = round((i_tf/i_cf)*len(topic_freq_list)/df,5)
            # scaling
            topic_freq_list[i][token] = round(i_tf*i_tf_idf,5)
    return topic_freq_list


def get_vocab(collection_freq,topic_freq_list,top_num=1000):
  topic_freq_list = freq_scaler(collection_freq,topic_freq_list)
  vocab = []
  for topic in topic_freq_list:
       sorted_list = sorted(topic.items(), key=lambda item: item[1],reverse=True)[:top_num]
       for word,frequency in sorted_list:
         vocab.append(word)
  return list(set(vocab))

def get_bow_vector(list_vocab,string,n_ary):
  vector_text=np.zeros(len(list_vocab))
  tw_tkns = get_tweet_token(string)
  if n_ary == 2 and len(tw_tkns)>0:
    tw_tkns = list(bigrams(tw_tkns))
  for i, word in enumerate(list_vocab):
    if word in tw_tkns:
      vector_text[i]=tw_tkns.count(word)
  return vector_text
  
def get_bow_features(any_set, vocab,n_ary=1):
  X=[]
  for instance in any_set:
    vector_instance=get_bow_vector(vocab,instance,n_ary)
    X.append(vector_instance)
  return X

