# %%
import numpy as np
import utils
import text_processing as tp
import collections
import functools
import operator
import random

import gensim
from gensim.models.keyedvectors import KeyedVectors

import matplotlib
import matplotlib.pyplot as plt
plt.style.use('seaborn-bright')

LOCAL_DIR = '../dataset/'
TRAIN_X_FNAME = "train_text.txt"
TRAIN_Y_FNAME = "train_labels.txt"
TEST_X_FNAME = "test_text.txt"
TEST_Y_FNAME = "test_labels.txt"
VAL_X_FNAME = "val_text.txt"
VAL_Y_FNAME = "val_labels.txt"


bin_label = [0,1]
NON_IRONY ="non_irony"
IRONY = "irony"
bin_cate = [NON_IRONY,IRONY]

#%%
"""

Loading raw data

"""
def load_rawsets():
    train_raw_X = utils.load_local_dataset(LOCAL_DIR+TRAIN_X_FNAME)
    train_y = utils.load_local_dataset(LOCAL_DIR+TRAIN_Y_FNAME,isNum=True)

    test_raw_X = utils.load_local_dataset(LOCAL_DIR+TEST_X_FNAME)
    test_y = utils.load_local_dataset(LOCAL_DIR+TEST_Y_FNAME,isNum=True)

    val_raw_X = utils.load_local_dataset(LOCAL_DIR+VAL_X_FNAME)
    val_y = utils.load_local_dataset(LOCAL_DIR+VAL_Y_FNAME,isNum=True)
    return {"train":[train_raw_X,train_y],"val":[val_raw_X,val_y],"test":[test_raw_X,test_y]}

        
    
#%%
raw_dicts = load_rawsets()
train_raw_X = raw_dicts["train"][0]
train_y = raw_dicts["train"][1]
print(len(train_raw_X))
print(len(train_y))

test_raw_X = raw_dicts["test"][0]
test_y = raw_dicts["test"][1]

val_raw_X = raw_dicts["val"][0]
val_y = raw_dicts["val"][1]
#%%
"""
Corpus token list and class token lists
"""
train_iron_tkns, train_non_iron_tkns  = tp.get_list_tokens(train_raw_X,train_y)
val_iron_tkns, val_non_iron_tkns = tp.get_list_tokens(val_raw_X,val_y)
test_iron_tkns, test_non_iron_tkns = tp.get_list_tokens(test_raw_X,test_y)
#print(train_iron_tkns)
#%%
"""

Featured text collection

"""
tra_iron_ftc = tp.get_featured_token_list(train_iron_tkns)
tra_non_iron_ftc = tp.get_featured_token_list(train_non_iron_tkns)

val_iron_ftc = tp.get_featured_token_list(val_iron_tkns)
val_non_iron_ftc = tp.get_featured_token_list(val_non_iron_tkns)

test_iron_ftc = tp.get_featured_token_list(test_iron_tkns)
test_non_iron_ftc = tp.get_featured_token_list(test_non_iron_tkns)

#%%

def get_ft_freq_dist(ft_token_list):
    ft_freq = {}
    for ft in tp.FEATURED_TYPE:
        ft_freq[ft]=tp.get_freq_dist(ft_token_list[ft])
    return ft_freq
# %%
tra_iron_ftc_freq = get_ft_freq_dist(tra_iron_ftc)
tra_non_iron_ftc_freq = get_ft_freq_dist(tra_non_iron_ftc)

#print(tra_iron_ftc_freq[tp.EMOJI].most_common(10))
#print(tra_non_iron_ftc_freq[tp.EMOJI].most_common(10))

#%%
def freq_analysis(title,label,freq_dist,number = 20):
    plt.figure(figsize=(10,7))
    common_ls = freq_dist[title].most_common(number)
    label_str = "ironic" if label==1 else "non-ironic"
    color = "lightblue" if label==1 else "lightgreen"
    y_tokens = []
    x_freqs = []
    print (common_ls)
    for freq in common_ls:
        y_tokens.append(freq[0])
        x_freqs.append(freq[1])
   # print(x_freqs[:50])
    plt.barh(y_tokens, x_freqs, color=color)
    plt.title(f'Most frequent {title} in {label_str} tweets')
    plt.gca().invert_yaxis()
    plt.show()

def freq_analysis_cmp(title,freq_dist,number = 10):
    pos_list = freq_dist[0][title].most_common(number)
    neg_list = freq_dist[1][title].most_common(number)
    if len(pos_list)<number:number = len(pos_list)
    x_pos_items = []
    y_pos_values =[] 
    x_neg_items=[]
    y_neg_values =[]

    for i in range(number):
        x_pos_items.append(pos_list[i][0])
        y_pos_values.append(pos_list[i][1])
        x_neg_items.append(neg_list[i][0])
        y_neg_values.append(neg_list[i][1])
  
    fig,(ax1,ax2) = plt.subplots(1,2,figsize=(15,5))
    ax1.bar(x_pos_items,y_pos_values,color="lightblue",edgecolor='black', linewidth=1.2)
    ax1.set_title("Ironic Tweets")
    
    ax2.bar(x_neg_items,y_neg_values,color="lightgreen",edgecolor='black', linewidth=1.2)
    ax2.set_title("Non-Ironic Tweets")
            
    plt.suptitle( f"{title} in tweets")
    plt.show()
#%%
freq_analysis_cmp(tp.EMOJI,freq_dist=[tra_iron_ftc_freq,tra_non_iron_ftc_freq])
freq_analysis_cmp(tp.HASHTAG,freq_dist=[tra_iron_ftc_freq,tra_non_iron_ftc_freq])
freq_analysis_cmp(tp.PUNCTUATION,freq_dist=[tra_iron_ftc_freq,tra_non_iron_ftc_freq])
freq_analysis_cmp(tp.EMOTIC,freq_dist=[tra_iron_ftc_freq,tra_non_iron_ftc_freq])
# %%
freq_analysis(tp.WORDS,0,tra_non_iron_ftc_freq)
freq_analysis(tp.WORDS,1,tra_iron_ftc_freq)

#%%
"""
Lexical features

"""
# whether includes featured tokens or not ?
corpus_tokens = train_iron_tkns + train_non_iron_tkns
corups_bigrams = tp.get_bigrams(corpus_tokens)
corpus_freq_dist = tp.get_freq_dist(corpus_tokens,dim=2)
corpus_freq_dict = tp.get_freq_dict(corpus_freq_dist)
#print (corpus_freq_dict)

# seperated freq dist for tf-idf calculation
iron_freq_dist = tp.get_freq_dist(train_iron_tkns,dim=2)
iron_freq_dict = tp.get_freq_dict(iron_freq_dist)
non_iron_freq_dist = tp.get_freq_dist(train_non_iron_tkns,dim=2)
non_iron_freq_dict = tp.get_freq_dict(non_iron_freq_dist)
#print(iron_freq_dist.most_common(200))
#print(non_iron_freq_dist.most_common(200))
bi_freq_list = [iron_freq_dict,non_iron_freq_dict]
corpus_bi_freq_dist = tp.get_freq_dist(list(corups_bigrams))
# %%
# vocabulary 
uni_vocab = tp.get_vocab(corpus_freq_dict,bi_freq_list)
bi_vocab = []
for bg in corpus_bi_freq_dist.most_common(1500):
    bi_vocab.append(bg[0])

train_uni_list = tp.get_bow_features(train_raw_X,uni_vocab) 
train_bi_list = tp.get_bow_features(train_raw_X,bi_vocab,n_ary=2)

val_uni_list = tp.get_bow_features(val_raw_X,uni_vocab) 
val_bi_list = tp.get_bow_features(val_raw_X,bi_vocab,n_ary=2)

test_uni_list = tp.get_bow_features(test_raw_X,uni_vocab) 
test_bi_list = tp.get_bow_features(test_raw_X,bi_vocab,n_ary=2)

# %%
"""
POS tagging
"""
train_pos_list = tp.load_pos(LOCAL_DIR+"train_text_pos.txt")
val_pos_list = tp.load_pos(LOCAL_DIR+"val_text_pos.txt")
test_pos_list = tp.load_pos(LOCAL_DIR+"test_text_pos.txt")
#%%

""""
sentiment
"""
#senti_lexi = tp.load_senti_lexicon() 
#emo_sent = tp.load_sentiemo_lexicon()
train_senti_vec = tp.get_senti_features(tp.get_full_tokens(train_raw_X))

val_senti_vec = tp.get_senti_features(tp.get_full_tokens(val_raw_X))

test_senti_vec = tp.get_senti_features(tp.get_full_tokens(test_raw_X))

# Convert features into DateFrame, and save as CSV in temp_data
# pos feature
train_pos_list_DF = utils.to_DataFrame(train_pos_list)
utils.save_DF_csv(train_pos_list_DF, "train_pos_list_DF")

val_pos_list_DF = utils.to_DataFrame(val_pos_list)
utils.save_DF_csv(val_pos_list_DF, "val_pos_list_DF")

test_pos_list_DF = utils.to_DataFrame(test_pos_list)
utils.save_DF_csv(test_pos_list_DF, "test_pos_list_DF")

# sentiment
train_senti_vec_DF = utils.to_DataFrame(train_senti_vec)
utils.save_DF_csv(train_senti_vec_DF, "train_senti_vec_DF")

val_senti_vec_DF = utils.to_DataFrame(val_senti_vec)
utils.save_DF_csv(val_senti_vec_DF, "val_senti_vec_DF")

test_senti_vec_DF = utils.to_DataFrame(test_senti_vec)
utils.save_DF_csv(test_senti_vec_DF, "test_senti_vec_DF")

# bi-grams
train_uni_list_DF = utils.to_DataFrame(train_uni_list)
utils.save_DF_csv(train_uni_list_DF, "train_uni_list_DF")

val_uni_list_DF = utils.to_DataFrame(val_uni_list)
utils.save_DF_csv(val_uni_list_DF, "val_uni_list_DF")

test_uni_list_DF = utils.to_DataFrame(test_uni_list)
utils.save_DF_csv(test_uni_list_DF, "test_uni_list_DF")

train_bi_list_DF = utils.to_DataFrame(train_bi_list)
utils.save_DF_csv(train_bi_list_DF, "train_bi_list_DF")

val_bi_list_DF = utils.to_DataFrame(val_bi_list)
utils.save_DF_csv(val_bi_list_DF, "val_bi_list_DF")

test_bi_list_DF = utils.to_DataFrame(test_bi_list)
utils.save_DF_csv(test_bi_list_DF, "test_bi_list_DF")

