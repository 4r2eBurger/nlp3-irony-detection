import os
import spacy
import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest

# Init tokeniser/lemmatiser/stopwords module
nlp = spacy.load("en_core_web_sm", exclude=['tok2vec', 'parser', 'ner'])

## FUNCTIONS ##
def get_text_tokens(list_of_texts):
    docs = list(nlp.pipe(list_of_texts)) # batch processing more efficient
    tokenised_articles = []
    for doc in docs:
        words = []
        for token in doc:
            if not ((token.is_stop) or (token.lemma_.isspace()) or (token.lemma_ in [".", ",", "-", "\"", "(", ")"])):
                words.append(token.lemma_)
        tokenised_articles.append(words)
    return tokenised_articles

def get_text_vector(vocab_list, text_tokens):
    vector = np.zeros(len(vocab_list))
    for i, word in enumerate(vocab_list):
        if word in text_tokens:
            vector[i]=text_tokens.count(word)
    return vector

def get_text_vectors(vocab_list, text_tokens_list):
    vectors_list = []
    counter = 0
    total = len(text_tokens_list)
    print("Progress out of", total)
    for text_tokens in text_tokens_list:
        text_vector = get_text_vector(vocab_list, text_tokens)
        vectors_list.append(text_vector)
        counter+=1
        print(counter, "\r", end="")
    print()
    return vectors_list

def get_vocab(token_articles):
    vocab = []
    for article in token_articles:
        for word in article:
            if word not in vocab:
                vocab.append(word)
    return vocab

def get_word_count_and_avg_len(text_tokens_list):
    word_counts = []
    avg_word_lens = []
    for text in text_tokens_list:
        word_count = len(text)
        word_counts.append([word_count])
        avg_word_len = 0
        for word in text:
            avg_word_len += len(word)
        avg_word_len /= word_count
        avg_word_lens.append([avg_word_len])
    return word_counts, avg_word_lens
## END FUNCTIONS ##

print("Reading in dataset..")
path = "bbc" # CHANGE PATH TO MAIN DATASET DIRECTORY HERE
topics = ["business", "entertainment", "politics", "sport", "tech"]
X_full_raw = []
y_full = []
for topic in topics:
    topic_path = os.path.join(path, topic)
    article_files = os.listdir(topic_path)
    for article_file in article_files:
        article_path = os.path.join(topic_path, article_file)
        try:
            with open(article_path) as f:
                raw_text = f.read()
                raw_text = raw_text.replace("Â£", "£") # Strange issue with this character in the dataset
                X_full_raw.append(raw_text)
                y_full.append(topic)
        except:
            print("An exception occurred trying to read an article - will continue with rest of articles")
print("Articles read in:", len(y_full))

# Reduce the test_size here to reduce the size of the full dataset
# X_temp_raw, X_full_raw, y_temp, y_full = train_test_split(X_full_raw, y_full, test_size=0.4, random_state=42)

# Split dataset 80% train / 10% dev / 10% test
# random_state set for repeatability
X_train_raw, X_test_raw, y_train, y_test = train_test_split(X_full_raw, y_full, test_size=0.2, random_state=42)
X_test_raw, X_dev_raw, y_test, y_dev = train_test_split(X_test_raw, y_test, test_size=0.5, random_state=42)

print("Tokenising, lemmatising, removing stopwords on train set..")
X_train_tokens = get_text_tokens(X_train_raw)

print("Creating vocabulary from train set..")
train_vocab = get_vocab(X_train_tokens)
print("Vocabulary size:", len(train_vocab))

print("Extracting features for train set..")
X_train_vectors = get_text_vectors(train_vocab, X_train_tokens)
word_counts, avg_word_lens = get_word_count_and_avg_len(X_train_tokens)
np.append(X_train_vectors, word_counts, axis=1) # append column-wise
np.append(X_train_vectors, avg_word_lens, axis=1)

print("Extracting features for dev set..")
X_dev_tokens = get_text_tokens(X_dev_raw)
X_dev_vectors = get_text_vectors(train_vocab, X_dev_tokens)
word_counts, avg_word_lens = get_word_count_and_avg_len(X_dev_tokens)
np.append(X_dev_vectors, word_counts, axis=1)
np.append(X_dev_vectors, avg_word_lens, axis=1)

print("\nTraining..")
list_num_features = [50,100,150,200,250,300,400,500,750,1000,1250,1500,1750,2000,2500,3000]
best_accuracy_dev = 0.0
for num_features in list_num_features:
    # Select num_features best features
    feat_sel = SelectKBest(chi2, k=num_features).fit(X_train_vectors, y_train)
    X_train = feat_sel.transform(X_train_vectors)

    clf = svm.SVC()
    clf.fit(X_train, y_train)
    
    # Transorm dev set to k features and make the prediction on this set
    X_dev = feat_sel.transform(X_dev_vectors)
    y_dev_pred = clf.predict(X_dev)

    # Get accuracy results of the classifier
    accuracy_dev = accuracy_score(y_dev, y_dev_pred)
    print("Accuracy with", num_features, "features:", round(accuracy_dev,3))

    if accuracy_dev >= best_accuracy_dev:
        best_accuracy_dev = accuracy_dev
        best_num_features = num_features
        best_features = feat_sel
        best_clf = clf

print("Best accuracy overall in the dev set is", round(best_accuracy_dev,3), "with", best_num_features, "features.")

print("\nTesting with test set uing best clf and best", best_num_features, "features..")
print("Extracting features for test set..")
X_test_tokens = get_text_tokens(X_test_raw)
X_test_vectors = get_text_vectors(train_vocab, X_test_tokens)
word_counts, avg_word_lens = get_word_count_and_avg_len(X_test_tokens)
np.append(X_test_vectors, word_counts, axis=1)
np.append(X_test_vectors, avg_word_lens, axis=1)

X_test = best_features.transform(X_test_vectors)

y_test_pred = best_clf.predict(X_test)
print("\n", classification_report(y_test, y_test_pred))
