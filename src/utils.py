import pickle
import os, os.path
import re
import random
import subprocess
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import itertools

import pandas as pd


def serialisation(file_name,data):
    # Open a file and use dump() 
    with open(file_name, 'wb') as file: 
        # A new file will be created 
        pickle.dump(data, file , pickle.HIGHEST_PROTOCOL)

def load(file_name):
    # Open the file in binary mode 
    with open(file_name, 'rb') as file:
     # Call load method to deserialze 
     data = pickle.load(file) 
     return data

# loading dataset from local directory
def load_local_dataset(dir,encoding='utf-8',isNum=False):
    raw_dataset = []
    dataset = []
    with open(dir,'r', encoding='utf-8',
                 errors='ignore') as f:
        raw_dataset = f.readlines()
    for line in raw_dataset:
        if line != '\n' and line != '': 
            line = line.replace('\n','')
            if isNum: line = int(line)
            dataset.append(line)
        
    return dataset
 
def dir_files_count(dir):
   return len([name for name in os.listdir(dir) if os.path.isfile(os.path.join(dir, name))])


def does_file_exist(filename, dir="./"):
    return os.path.exists(os.path.join(dir,filename))

def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

# Convert features into DateFrame for modelling
def to_DataFrame(vec_list: list) -> pd.DataFrame:
    temp_vec = []
    for vec in vec_list:
        temp_vec.append(vec.tolist())
    vec_DF = pd.DataFrame(temp_vec)
    return vec_DF

def save_DF_csv(feature_DF: pd.DataFrame, csv_name: str):
    dir = "./temp_data/" + csv_name.strip() + ".csv"
    feature_DF.to_csv(dir, index=False)

"""
confusion matrix plotting
"""
def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.show()