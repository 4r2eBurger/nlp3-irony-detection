# CMT316 NLP3 Irony Detection

## Problem Sheets

- [General Requirements](https://docs.google.com/document/d/1JArAZAD37NcUTcosviHJeQvzd8FoHO3oFg-M1ul5XZs/edit)
- [Group Project Description](https://docs.google.com/document/d/1P8jc81L_HW3DDdZaIMfMcekrPeDYuBm-2qTC7knbKV8/edit?usp=sharing )
- [Group Report Guidelines](https://docs.google.com/document/d/1ku-K6mBH8-Wdfy_Dz_gvpReDv6knWCFxq4rsMDCrPHY/edit)

## Useful Link

- [**TweetEval**](https://github.com/cardiffnlp/tweeteval)(Datasets, pretrained models, research papers) 

- [**Huggingface**](https://github.com/huggingface/transformers)(Pretrained models, transformer examples)

## Pretrained Embeddings
- [20 million tweets, 300 dim](https://drive.google.com/file/d/1TTO2h2ltTgeYzkvHIzS3TMuUV4RdPTo1/view?usp=sharing)
- [400 million tweets, 400 dim](https://drive.google.com/file/d/1-Hi2LsUCC2MqCOKebJ5nIa4of0XW-UN2/view?usp=sharing) (crashed on colab)
- [LSTM + Dense Layer using the first word embedding](https://colab.research.google.com/drive/1BKIbPuvVizM8KpPy7bDqRbpPtw5WV1T0?usp=sharing)

## Pretrained Models
- [simpletransformers RoBERTa](https://colab.research.google.com/drive/1NIw_hHIHe2FOOzSpCH6Ee_0T142gnKyt?usp=sharing)

## Contacts

### Literature Review & Prepocessing
- [Zihao Wang](mailto:wangz144@cardiff.ac.uk)
- [Zhiliang Xiang](mailto:xiangz6@cardiff.ac.uk)

### Descriptive & Error Analysis
- [Harry Dafas](mailto:dafash@cardiff.ac.uk)
- [Hao Wang](mailto:wangh90@cardiff.ac.uk)
 
### Implementation & Result
- [Bartosz Borne](mailto:borneb@cardiff.ac.uk) 
- [Qin Liu](mailto:liuq39@cardiff.ac.uk)
 


