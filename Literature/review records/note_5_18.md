### Question
- emojis, emoticons, hashtags are extracted, should we include them into BOW representation?
- n-grams character?
- sentiment feature engineering on raw text or preprocessed text?
- negation of sentiments
- emoji ploting

### [POS tagger](http://www.cs.cmu.edu/~ark/TweetNLP/#pos_down)
- [paper](http://www.cs.cmu.edu/~ark/TweetNLP/gimpel+etal.acl11.pdf)
- 25 different tags specifically used in tweet
- problem: low accuracy on emoji, consecutive emojis and hashtags are counted as one.
- Presence of clash between 2 verb in a tweet

### Sentiment features
- sentiment lexicon: 
	- [AFINN](http://www2.imm.dtu.dk/pubdb/pubs/6010-full.html)
	- [General Inquirer](http://www.wjh.harvard.edu/~inquirer/homecat.htm)
	- [MPQA](http://mpqa.cs.pitt.edu/lexicons/subj_lexicon/)
	- [NRC Emotion Lexicon](https://www.saifmohammad.com/WebPages/NRC-Emotion-Lexicon.htm) The NRC Emotion Lexicon is a list of English words and their associations with eight basic emotions (anger, fear, anticipation, trust, surprise, sadness, joy, and disgust) and two sentiments (negative and positive). 
	- Liu’s opinion lexicon
	- Hogenboom’s emoticon lexicon
	- [Kralj’s emoji sentiment lexicon](https://www.clarin.si/repository/xmlui/handle/11356/1048)
- the number of positive, negative and neutral lexicon words averaged over text length;
- the overall tweet polarity, i.e. the sum of the values of the identified sentiment words;
- the difference between the highest positive and lowest negative sentiment;
- a binary feature indicating the presence of a polarity contrast (i.e. the tweet contains at least one positive and negative sentiment word).


### Others
- [reference ](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.207.5253&rep=rep1&type=pdf)
  >Sarcasm transforms the polarity of an apparently positive or negative utterance into
its opposite. 

