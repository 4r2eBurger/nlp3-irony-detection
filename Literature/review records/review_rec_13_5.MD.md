## Motivations

- >Identifying the ironic texts can help to understand the social web better and has many applications such as sentiment analysis
- >Irony detecting techniques are important to improve the performance of sentiment
  analysis.  For example, the tweet “Monday mornings are my fave:)# not” is an irony with negative sentiment, but it will be probably classified as a positive one by a standard sentiment analysis
  model.
- > Many existing methods are aimed to detect irony in tweets with explicit irony related hashtags. To fill this gap the SemEval-2018 task 3 aims to detect irony of tweets without explicit irony hashtags

- Many works sperated features into different groups, conduct training on different combination of feature groups and different models. In the report we investigate multiple groups of useful features and experiment them with existing prominent solutions.

## Feature Extraction Approaches

### Reference: [THU NGN at SemEval-2018 Task 3](https://www.aclweb.org/anthology/S18-1006.pdf)

Comment: focus on model buiding, lack of deliberate feature engineering. Might be improved by adding more related features.

#### Word Embeddings (with pre-trained models)
#### Sentence Embeddings


### Reference: [Francesco et al](http://socsci-dev.ss.uci.edu/~lpearl/courses/readings/BarbieriSaggion2014_DetectingIrony.pdf)



#### Rarity Frequency (Unexpectedness)
- Showing that unexpectedness is the feature most related to situational
  
  > The idea is that the use of many words commonly used in English (i.e. high frequency in ANC) and only a few terms rarely used in English (i.e. low frequency in ANC) in the same sentence creates imbalance that may cause unexpectedness, since within a single tweet only one kind of register is expected.
  
  **Comment**: doubt it, no supported literature found

#### Tweets Structure
- if it is long or short (length), if it contains long or short words (mean of word length), and also what kind of punctuation is used (exclamation marks, emoticons,etc.).
- Ironic tweets tend to be longer, contain certain kinds of punctuation and specific emoticons/emojis
- reflection of structure: 
	- written style: number of words, number of characters, POS frequency (scaled by sum)
	- punctuation: sum of the number of commas, full stops, ellipsis and exclamation that a tweet  presents
	- laughing:  sum of all the internet laughs, denoted with hahah, *lol*, *rofl*, and *lmao* that we consider as a new form of punctuation: instead of using many exclamation marks internet users may use the sequence *lol* (i.e. laughing out loud) or just type *hahaha*.

#### **Sentiment Intensity**
- a way to measure sentiment contrast, polarity contrast
- mainly on adjectives and adverbs:
  - adj (adv) tot: The sum of the AdjScale scores of all the adjectives in the tweet
  - adj (adv) mean: adj tot divided by the number of adjectives in the tweet
  - adj (adv) max: The maximum AdjScale score within a single tweet
  - adj (adv) gap: s the difference between adj max and adj mean
  - (? to experiment) the difference between max and min

#### Synonyms
