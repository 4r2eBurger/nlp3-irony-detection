
### Dataset
- noise: '@users', urls, stopwords
- emojis: implict indicator, dense distribution, e.g. 😂
- hashtags: implicit indicator, sparse disitribution, e.g. #suck, #WTF

### Related paper
- 1. [Irony Detection in English Tweets](https://git.cardiff.ac.uk/c2028447/irony_detection/-/blob/master/Literature/(OriginalPaper)Irony%20Detection%20in%20English%20Tweets.pdf)(mainly about exisiting solution evaluation)

- 2. [Exploring the fine-grained analysis and automatic detection of irony on Twitter](https://git.cardiff.ac.uk/c2028447/irony_detection/-/blob/master/Literature/Exploring%20TheFine-grained%20Irony%20Analys.pdf)(gives defination of irony and evaluation mechanism of labelling, provides ML and DL solutions)

### Irony Defining and Modelling
### Irony Categories
- situational irony: situations that fails to meet some expectation
	e.g:  firefighters who have a fire in their kitchen while they are out to answer a fire alarm would be a typically ironic situation.
- verbal irony: verbal polarity contrast, saying the opposite of what is meant
#### how to identifiy above (irony evaluation)? 
- ironic by means of a clash
- hashtag indication
- harness
- polarity contrast, opposite sentiments
- details in  [paper2](https://git.cardiff.ac.uk/c2028447/irony_detection/-/blob/master/Literature/Exploring%20TheFine-grained%20Irony%20Analys.pdf)

### Features and Methods
#### Features
- combination of BOW (word frequency) and part-of-speech
- syntatic features (e.g. number of interjections)
- sentiment (positive or negative word values)
- word embeddings
- emojis (numeric mapping, need further relevance analysis on training data, e.g: propotion in irony and non-irony tweets) 
- hashtags (need further relevance analysis on training data, e.g: propotion in irony and non-irony tweets)

#### Methods
- SVM with context and user-based features
- LSTM with word embeddings
- Performance evaluation of more existing solutions, see [paper 1](https://git.cardiff.ac.uk/c2028447/irony_detection/-/blob/master/Literature/(OriginalPaper)Irony%20Detection%20in%20English%20Tweets.pdf).
