#### Question
- whether pretrained wemb or pretrained model that trained on a large amount of data will bring noise to specific task prediction or not ?

- sequence index of embedding layer, use the vocab of pretrained embedding or vocab built on dataset?


#### [A Report on the 2020 Sarcasm Detection Shared Task](https://www.aclweb.org/anthology/2020.figlang-1.1.pdf)

#### Previous attempts
- (Initial approaches) feature-based machine learning model
- deep learning approaches
- contextual information for irony and sarcasm analysis

#### [Gentle Intro of Transformer with Codes] (http://nlp.seas.harvard.edu/2018/04/03/attention.html)

####[TWEETEVAL](https://arxiv.org/pdf/2010.12421.pdf)
 exploring whether a Twitter-specific LM should be trained on Twitter only or if it should be initialized with weights learned during pre-training on standard corpora, and then be trained on Twitter
- existing pretrained LM
- existing architecture training from scratch using only Twitter data
- starting with an original pretrained LM and continue to train with Twitter data

##### Pretrained model bias
- pretrained model based on a large amount of data, records in dataset might already used in pretrained model training.
- learning on different types of text corpora make the models more robust and knowledgeable about the world? 