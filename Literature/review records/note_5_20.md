### word embeddings and language models

- [word embedding and NN](https://www.kdnuggets.com/2018/05/contribution-neural-networks-word-embeddings-natural-language-processing.html)
- [Understanding Word Embeddings and Language Models](https://link.springer.com/content/pdf/10.1007/978-3-030-44830-1_3.pdf)
- [BERT Explained: State of the art language model for NLP](https://towardsdatascience.com/bert-explained-state-of-the-art-language-model-for-nlp-f8b21a9b6270#:~:text=BERT%20(Bidirectional%20Encoder%20Representations%20from,researchers%20at%20Google%20AI%20Language.&text=The%20paper's%20results%20show%20that,than%20single%2Ddirection%20language%20models.)