### Identified issue in [THU_NGN SemEval Task3](https://www.aclweb.org/anthology/S18-1006.pdf)
- showed possiblity of improving model performance by introducing sentiment and POS features
- utilise [Affective Tweets](https://affectivetweets.cms.waikato.ac.nz/#tokenizers) to do sentiment extraction, [CMU Tweet NLP Tool](http://www.cs.cmu.edu/~ark/TweetNLP/) for POS
- but both above lack of emoji information, (1) doesn't include emojis, (2) outdated, has low conifidence on emoji pos tagging
- solution: combine (1) with [emoji ranking](http://kt.ijs.si/data/Emoji_sentiment_ranking/index.html), add new emoji pos tag in output of (2)

### problems in feature engineering
- included featured text in bigram, problem: contain meaningless element like `(',','i'm')`(combination of punctuation and text)
- consecutivly repeated elements might indicate irony, like😂😂😂
- frequent vocabs in both classes have overlapped words, need to weight with TF-IDF
